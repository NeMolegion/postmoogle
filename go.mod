module gitlab.com/etke.cc/postmoogle

go 1.21.0

toolchain go1.22.0

// replace gitlab.com/etke.cc/linkpearl => ../linkpearl

require (
	github.com/archdx/zerolog-sentry v1.8.2
	github.com/emersion/go-msgauth v0.6.8
	github.com/emersion/go-sasl v0.0.0-20231106173351-e73c9f7bad43
	github.com/emersion/go-smtp v0.21.0
	github.com/fsnotify/fsnotify v1.7.0
	github.com/gabriel-vasile/mimetype v1.4.3
	github.com/getsentry/sentry-go v0.27.0
	github.com/jhillyerd/enmime v1.2.0
	github.com/kvannotten/mailstrip v0.0.0-20200711213611-0002f5c0467e
	github.com/lib/pq v1.10.9
	github.com/mattn/go-sqlite3 v1.14.22
	github.com/mcnijman/go-emailaddress v1.1.1
	github.com/mileusna/crontab v1.2.0
	github.com/raja/argon2pw v1.0.2-0.20210910183755-a391af63bd39
	github.com/rs/zerolog v1.32.0
	gitlab.com/etke.cc/go/env v1.1.0
	gitlab.com/etke.cc/go/fswatcher v1.0.0
	gitlab.com/etke.cc/go/healthchecks/v2 v2.0.0
	gitlab.com/etke.cc/go/mxidwc v1.0.0
	gitlab.com/etke.cc/go/psd v1.1.1
	gitlab.com/etke.cc/go/secgen v1.2.0
	gitlab.com/etke.cc/go/validator v1.0.7
	gitlab.com/etke.cc/linkpearl v0.0.0-20240316115913-106577b88942
	golang.org/x/exp v0.0.0-20240404231335-c0f41cb1a7a0
	maunium.net/go/mautrix v0.18.0
)

require (
	blitiri.com.ar/go/spf v1.5.1 // indirect
	github.com/buger/jsonparser v1.1.1 // indirect
	github.com/cention-sany/utf7 v0.0.0-20170124080048-26cad61bd60a // indirect
	github.com/gogs/chardet v0.0.0-20211120154057-b7413eaefb8f // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/hashicorp/golang-lru/v2 v2.0.7 // indirect
	github.com/jaytaylor/html2text v0.0.0-20230321000545-74c2419ad056 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mattn/go-runewidth v0.0.15 // indirect
	github.com/mikesmitty/edkey v0.0.0-20170222072505-3356ea4e686a // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	github.com/ssor/bom v0.0.0-20170718123548-6386211fdfcf // indirect
	github.com/tidwall/gjson v1.17.1 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
	github.com/tidwall/sjson v1.2.5 // indirect
	github.com/yuin/goldmark v1.7.1 // indirect
	gitlab.com/etke.cc/go/trysmtp v1.1.3 // indirect
	go.mau.fi/util v0.4.1 // indirect
	golang.org/x/crypto v0.22.0 // indirect
	golang.org/x/net v0.24.0 // indirect
	golang.org/x/sys v0.19.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
